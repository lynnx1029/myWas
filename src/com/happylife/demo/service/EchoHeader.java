package com.happylife.demo.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.happylife.demo.servlet.http.HttpRequest;
import com.happylife.demo.servlet.http.HttpResponse;
import com.happylife.demo.servlet.http.SimpleServlet;
import com.sun.net.httpserver.Headers;

public class EchoHeader implements SimpleServlet {
    @Override
    public void service(HttpRequest req, HttpResponse res) {
        Headers headers = req.getHeaders();
        String requestMethod = req.getMethod();
        System.out.println("aa " + requestMethod);
        Set<Map.Entry<String, List<String>>> entries = headers.entrySet();
        String response = "";
        for (Map.Entry<String, List<String>> entry : entries)
            response += entry.toString() + "\n";
        try {
            res.startSending(200, response.length());
            OutputStream os = res.getBodyOutputStream();
            os.write(response.toString().getBytes());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
