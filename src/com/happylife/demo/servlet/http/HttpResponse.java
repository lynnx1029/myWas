package com.happylife.demo.servlet.http;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.Headers;

public interface HttpResponse {
    void addHeader(String key, String val);
    Headers getHeaders();
    OutputStream getBodyOutputStream();
    void startSending(int rCode, long length) throws IOException;
}
