package com.happylife.demo.servlet.http;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.URI;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpPrincipal;

public interface HttpRequest {
    Object getAttribute(String name);
    InetSocketAddress getLocalAddress();
    InetSocketAddress getRemoteAddress();
    HttpPrincipal getPrincipal();
    String getProtocol();
    String getMethod();
    URI getURI();
    Headers getHeaders();
    InputStream getBodyStream();
}
