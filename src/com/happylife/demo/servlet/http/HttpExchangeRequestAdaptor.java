package com.happylife.demo.servlet.http;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.URI;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpPrincipal;

public class HttpExchangeRequestAdaptor implements HttpRequest {
    private HttpExchange exchange;
    
    public HttpExchangeRequestAdaptor(HttpExchange exchange) {
        this.exchange = exchange;
    }
    
    @Override
    public Object getAttribute(String name) {
        return exchange.getAttribute(name);
    }

    @Override
    public InetSocketAddress getLocalAddress() {
        return exchange.getLocalAddress();
    }

    @Override
    public InetSocketAddress getRemoteAddress() {
        return exchange.getRemoteAddress();
    }

    @Override
    public HttpPrincipal getPrincipal() {
        return exchange.getPrincipal();
    }

    @Override
    public String getProtocol() {
        return exchange.getProtocol();
    }

    @Override
    public String getMethod() {
        return exchange.getRequestMethod();
    }

    @Override
    public URI getURI() {
        return exchange.getRequestURI();
    }

    @Override
    public Headers getHeaders() {
        return exchange.getRequestHeaders();
    }

    @Override
    public InputStream getBodyStream() {
        return exchange.getRequestBody();
    }
}
