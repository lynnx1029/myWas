package com.happylife.demo.servlet.http;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class HttpExchangeResponseAdaptor implements HttpResponse {
    private HttpExchange exchange;
    
    public HttpExchangeResponseAdaptor(HttpExchange exchange) {
        this.exchange = exchange;
    }
    
    @Override
    public void addHeader(String key, String val) {
        exchange.getResponseHeaders().add(key, val);
    }

    @Override
    public Headers getHeaders() {
        return exchange.getResponseHeaders();
    }

    @Override
    public OutputStream getBodyOutputStream() {
        return exchange.getResponseBody();
    }

    @Override
    public void startSending(int rCode, long length) throws IOException {
        exchange.sendResponseHeaders(rCode, length);
    }
}
